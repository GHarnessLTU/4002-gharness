var monitors = [
    {
        title: "19 inch",
        brandName: "Acer",
        refreshRate: "60hz",
        responseTime: "5ms",
        askingPrice: 50.00,
        imageUrls: ["19inchmonitor.jpg"]       
    },
    {
        title: "24 inch",
        brandName: "BenQ",
        refreshRate: "60hz",
        responseTime: "1ms",
        askingPrice: 100.00,
        imageUrls: ["24inchmonitor.jpg"]       
    },
    {
        title: "27 inch",
        brandName: "Lenovo",
        refreshRate: "144hz",
        responseTime: "1ms",
        askingPrice: 150.00,
        imageUrls: ["27inchmonitor.jpg"]       
    }
];