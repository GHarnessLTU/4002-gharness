var resultsContainer = document.getElementById("monitorResults");

var html = "";

for (var i=0;i<monitors.length;i++) {
    if (i % 3 == 0 || i == 0) {
        html += "<div class='row'>";
    }
    html += "<div class='col-md-4'><div class='monitorSizes'>";
    html += "<h3>" + monitors[i].title + "</h3>";
    html += "<h4>" + monitors[i].brandName + "</h4>";
    html += "<div class='monitorImgs'>";
    html += "<img src='images/" + monitors[i].imageUrls[0] + "' height=180 />";
    html += "</div>"
    html += "</div></div>";
    if ((i+1) % 3 == 0) {
        html += "</div>";
    }
}
resultsContainer.innerHTML = html;


